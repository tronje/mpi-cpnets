//! The marking of a net maps its places to a collection of tokens.
//!
//! In order to create a new marking, use either `Net::empty_marking`, or `Marking::empty_for` with
//! a given net. This is to avoid logic errors where a marking is used for simulations with a net,
//! but the marking was created for a different net. Simulations may raise `Error::InvalidMarking`,
//! but only if they attempt to access tokens for a place that is not contained in the `Marking`.

use crate::error::{Error, Result};
use crate::net::Net;
use crate::place::Place;
use crate::token::Token;
use std::collections::hash_map::DefaultHasher;
use std::fmt;
use std::hash::{Hash, Hasher};

fn hash<H: Hash>(thing: &H) -> u64 {
    let mut hasher = DefaultHasher::new();
    thing.hash(&mut hasher);
    hasher.finish()
}

#[derive(Debug, Clone)]
pub(crate) struct TokenVec {
    hashes: Vec<u64>,
    inner: Vec<Token>,
}

impl TokenVec {
    fn new() -> Self {
        Self {
            hashes: Vec::new(),
            inner: Vec::new(),
        }
    }

    pub(crate) fn push(&mut self, token: Token) {
        self.hashes.push(hash(&token));
        self.hashes.sort();
        self.inner.push(token);
    }

    pub(crate) fn remove(&mut self, idx: usize) -> Token {
        let token = self.inner.remove(idx);
        let h = hash(&token);
        let idx = self.hashes.binary_search(&h).unwrap();

        // don't use swap_remove to keep sorted!
        self.hashes.remove(idx);

        token
    }

    fn iter(&self) -> impl Iterator<Item = &Token> {
        self.inner.iter()
    }
}

impl std::ops::Deref for TokenVec {
    type Target = [Token];

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl Hash for TokenVec {
    fn hash<H>(&self, state: &mut H)
    where
        H: Hasher,
    {
        self.hashes.hash(state);
    }
}

impl PartialEq for TokenVec {
    fn eq(&self, other: &Self) -> bool {
        self.hashes == other.hashes
    }
}

impl Eq for TokenVec {}

/// The marking of a net maps places to token sets.
///
/// Since place names must be unique, their names are used as the key.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Marking {
    // This should really be a HashMap, but it needs to implement Hash for use in the StateGraph,
    // and HashMap does not implement Hash. So this workaround is used instead.
    inner: Vec<(Place, TokenVec)>,
}

impl Marking {
    /// Create an empty marking for the given net.
    pub fn empty_for(net: &Net) -> Self {
        let mut inner = Vec::new();
        for place in net.places.iter() {
            inner.push((place.clone(), TokenVec::new()));
        }

        let mut this = Self { inner };
        this.sort();
        this
    }

    fn sort(&mut self) {
        // Keep sorted to ensure that equivalent Markings are also equal according to their
        // PartialEq implementations.
        self.inner.sort_by(|(a, _), (b, _)| a.name().cmp(b.name()));
    }

    /// Add a token to the place with the name `name`.
    pub fn add(&mut self, name: &str, token: Token) -> Result<()> {
        for (place, tokens) in self.inner.iter_mut() {
            if place.name() == name {
                tokens.push(token);
                return Ok(());
            }
        }

        Err(Error::InvalidMarking)
    }

    /// Add multiple tokens to the place with the name `name`.
    pub fn extend<I>(&mut self, name: &str, tokens: I) -> Result<()>
    where
        I: IntoIterator<Item = Token>,
    {
        for (place, present_tokens) in self.inner.iter_mut() {
            if place.name() == name {
                for token in tokens {
                    present_tokens.push(token);
                }
                return Ok(());
            }
        }

        Err(Error::InvalidMarking)
    }

    /// Extend place `place` with the tokens `tokens`, using the builder pattern.
    ///
    /// # Examples
    ///
    /// ```
    /// # use mpi_cpnets::prelude::*;
    /// # fn main() -> mpi_cpnets::error::Result<()> {
    /// let mut net = Net::new("N");
    /// net.add_places(["P0", "P1", "P2"])?;
    ///
    /// let mut marking = Marking::empty_for(&net)
    ///     .with("P0", [Token::InitMarker])?
    ///     .with("P1", [Token::new_send(0, 1, 0, "MPI_DOUBLE")])?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn with<const N: usize>(mut self, place: &str, tokens: [Token; N]) -> Result<Self> {
        self.extend(place, tokens)?;
        Ok(self)
    }

    pub(crate) fn get(&self, name: &str) -> Result<&[Token]> {
        for (place, tokens) in self.inner.iter() {
            if place.name() == name {
                return Ok(tokens);
            }
        }

        Err(Error::InvalidMarking)
    }

    pub(crate) fn get_mut(&mut self, name: &str) -> Result<&mut TokenVec> {
        for (place, tokens) in self.inner.iter_mut() {
            if place.name() == name {
                return Ok(tokens);
            }
        }

        Err(Error::InvalidMarking)
    }
}

impl fmt::Display for Marking {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut first_place = true;

        for (place, tokens) in self.inner.iter() {
            if first_place {
                write!(f, "{}: [", place.name())?;
                first_place = false;
            } else {
                write!(f, ", {}: [", place.name())?;
            }

            let mut first_token = true;

            for token in tokens.iter() {
                if first_token {
                    write!(f, " {}", token)?;
                    first_token = false;
                } else {
                    write!(f, ", {}", token)?;
                }
            }
            f.write_str(" ]")?;
        }

        Ok(())
    }
}
