//! A place in a CPN.

use std::hash::{Hash, Hasher};

/// A place in a CPN.
#[derive(Debug, Clone)]
pub struct Place {
    name: String,
}

impl Place {
    /// Construct a new place with the given name
    pub fn new<S>(name: S) -> Self
    where
        S: Into<String>,
    {
        Self { name: name.into() }
    }

    /// Get a reference to the name of this place.
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl PartialEq for Place {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Place {}

impl Hash for Place {
    fn hash<H>(&self, state: &mut H)
    where
        H: Hasher,
    {
        state.write(self.name.as_bytes());
    }
}

impl From<&str> for Place {
    fn from(s: &str) -> Self {
        Self::new(s)
    }
}

impl From<String> for Place {
    fn from(s: String) -> Self {
        Self { name: s }
    }
}
