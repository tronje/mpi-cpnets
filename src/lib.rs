//! A Colored Petri Net library.

pub mod arc;
pub mod error;
pub mod expression;
pub mod marking;
pub mod net;
pub mod place;
pub mod prelude;
pub mod state_graph;
pub mod token;
pub mod transition;
