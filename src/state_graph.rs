//! State space analysis.

use crate::error::Result;
use crate::marking::Marking;
use crate::net::state_iterator::StateIterator;
use crate::prelude::*;
use std::collections::HashSet;
use std::rc::Rc;

/// A state graph of a CPN.
///
/// Its contained states are instances of `Marking`, since a marking completely describes the state
/// of a CPN.
#[derive(Debug)]
pub struct StateGraph {
    net: Net,
    vertices: HashSet<Rc<Marking>>,
    edges: HashSet<(Rc<Marking>, Rc<Marking>)>,
    terminal_states: HashSet<Rc<Marking>>,
    dead_transitions: HashSet<String>,
}

impl StateGraph {
    /// Create a new, empty `StateGraph` with the provided `net` as its initial state.
    pub fn init(net: Net) -> Self {
        let mut dead_transitions = HashSet::new();
        for t in &net.transitions {
            dead_transitions.insert(t.name.clone());
        }

        Self {
            net,
            vertices: HashSet::new(),
            edges: HashSet::new(),
            terminal_states: HashSet::new(),
            dead_transitions,
        }
    }

    /// Name of the net that this graph was constructed from.
    pub fn name(&self) -> &str {
        self.net.name()
    }

    /// Populate the graph by simulating the net it was initialized with.
    pub fn compute(mut self, initial_marking: Marking) -> Result<Self> {
        self.vertices.clear();
        self.vertices.insert(Rc::new(initial_marking));

        let mut states = Vec::new();
        states.push(self.vertices.iter().next().unwrap().clone());

        while let Some(state) = states.pop() {
            let next_states = StateIterator::new(&self.net, &state);
            for result in next_states {
                let (next_state, transition_name) = result?;
                self.dead_transitions.remove(&transition_name);

                let next_state = Rc::new(next_state);
                let newly_inserted = self.vertices.insert(next_state.clone());

                self.edges.insert((state.clone(), next_state.clone()));
                self.terminal_states.remove(&state);

                if newly_inserted {
                    states.push(next_state.clone());
                    self.terminal_states.insert(next_state);
                }
            }
        }

        Ok(self)
    }

    /// The number of vertices in the graph.
    pub fn order(&self) -> usize {
        self.vertices.len()
    }

    /// The number of edges in the graph.
    pub fn size(&self) -> usize {
        self.edges.len()
    }

    /// An iterator over all contained states (markings).
    pub fn states(&self) -> impl Iterator<Item = &Rc<Marking>> {
        self.vertices.iter()
    }

    /// Returns an iterator over all states that have exactly zero outgoing edges.
    ///
    /// This means these states are terminal and the net could "end up" in any of these after a
    /// simulation.
    pub fn terminal_states(&self) -> impl Iterator<Item = &Rc<Marking>> {
        self.terminal_states.iter()
    }

    /// Whether there is at least one state that is terminal, i.e. has no outgoing edges.
    pub fn terminates(&self) -> bool {
        self.terminal_states().next().is_some()
    }

    /// Returns a vector of names for transitions that have never fired.
    pub fn dead_transitions(&self) -> &HashSet<String> {
        &self.dead_transitions
    }
}
