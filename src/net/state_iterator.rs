use crate::error::Error;
use crate::error::Result;
use crate::prelude::*;
use std::slice::Iter;

/// An iterator over all subsequent states of a given net.
///
/// The iterator's `next` function returns a tuple of type `(Net, String)`; the `Net` is the
/// subsequent state, while the `String` is the name of the transition that fired to create this
/// state.
pub(crate) struct StateIterator<'a, 'b> {
    marking: &'b Marking,
    inner: Iter<'a, Transition>,
}

impl<'a, 'b> StateIterator<'a, 'b> {
    pub(crate) fn new(net: &'a Net, marking: &'b Marking) -> Self {
        Self {
            marking,
            inner: net.transitions.iter(),
        }
    }
}

impl<'a, 'b> Iterator for StateIterator<'a, 'b> {
    type Item = Result<(Marking, String)>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let t = self.inner.next()?;

            match t.can_fire(self.marking) {
                Ok(true) => {}
                Ok(false) => continue,
                Err(e) => return Some(Err(e)),
            };

            let mut new_marking = self.marking.clone();
            match t.fire(&mut new_marking) {
                Ok(_) => {}
                Err(Error::GuardCheckFailed) | Err(Error::TokenNotFound(_)) => continue,
                Err(e) => return Some(Err(e)),
            }

            return Some(Ok((new_marking, t.name.clone())));
        }
    }
}
