//! Arc expressions

use crate::error::Result;
use crate::token::Token;

/// An expression that just passes along the token bound to the name.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PassThroughExpression {
    pub binding_name: String,
}

/// An expression that generates a new token
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GeneratorExpression {
    pub token: Token,
}

/// An arc expression.
///
/// Can either pass tokens through to the next place, or discard its token and generate a new one.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Expression {
    PassThrough(PassThroughExpression),
    Generator(GeneratorExpression),
}

impl Expression {
    pub fn passthrough<S>(binding_name: S) -> Self
    where
        S: Into<String>,
    {
        Self::PassThrough(PassThroughExpression {
            binding_name: binding_name.into(),
        })
    }

    pub fn generator(token: Token) -> Self {
        Self::Generator(GeneratorExpression { token })
    }

    pub(crate) fn evaluate(&self, token: Option<&Token>) -> Result<Token> {
        match self {
            Self::PassThrough(_) => Ok(token.unwrap().clone()),
            Self::Generator(exp) => Ok(exp.token.clone()),
        }
    }
}
