//! Arc connecting places and transitions

use crate::error::{Error, Result};
use crate::expression::Expression;
use crate::token::Token;

fn contains_whitespace(s: &str) -> bool {
    s.chars().any(char::is_whitespace)
}

/// Helper enum to allow `ArcSpec` to not need a type parameter.
#[derive(Debug)]
pub enum BindingOrExpression<'a> {
    /// Binding, for arcs from places to transitions.
    Binding(&'a str),

    /// Expression, for arcs from transitions to places.
    Expression(Expression),
}

impl<'a> From<&'a str> for BindingOrExpression<'a> {
    fn from(s: &'a str) -> Self {
        Self::Binding(s)
    }
}

impl From<Expression> for BindingOrExpression<'_> {
    fn from(e: Expression) -> Self {
        Self::Expression(e)
    }
}

/// An arc specification, to simplify adding an `Arc` to a net.
#[derive(Debug)]
pub struct ArcSpec<'a> {
    /// The place or transition from which the arc shall originate.
    pub from: &'a str,

    /// The place or transition at which the arc shall terminate.
    pub to: &'a str,

    /// The arc expression or variable binding.
    pub inner: BindingOrExpression<'a>,
}

/// The direction of an arc.
///
/// Arcs in CPNs can connect a place to a transition, or vice versa.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Direction {
    /// The arc takes tokens out of its [`Place`] and forwards them to its [`Transition`].
    PlaceToTransition,

    /// The arc receives tokens from its [`Transition`] and deposits them in its [`Place`].
    TransitionToPlace,
}

/// Inner type for an `Arc` from a place to a transition.
#[derive(Debug)]
pub struct PlaceToTransition {
    binding: String,
}

impl PlaceToTransition {
    /// Create a new place-to-transition arc with the variable name `binding`.
    pub(crate) fn new(place: String, binding: String) -> Result<Arc<Self>> {
        // TODO maybe do more complete checks here for a valid binding name
        if contains_whitespace(&binding) {
            Err(Error::InvalidBinding)
        } else {
            let arc = Arc::new(place, Self { binding });
            Ok(arc)
        }
    }
}

/// Inner type for an `Arc` from a transition to a place.
#[derive(Debug)]
pub struct TransitionToPlace {
    expression: Expression,
}

impl TransitionToPlace {
    /// Create a new transition-to-place arc with the expression `expression`.
    pub(crate) fn new(place: String, expression: Expression) -> Arc<Self> {
        Arc::new(place, Self { expression })
    }
}

/// An arc connecting places and transitions.
///
/// Keeps track of its place so that tokens can be taken/deposited from/into it.
#[derive(Debug)]
pub(crate) struct Arc<T> {
    pub(crate) place: String,
    pub(crate) inner: T,
}

impl<T> Arc<T> {
    pub(crate) fn new(place: String, inner: T) -> Self {
        Self { place, inner }
    }
}

impl Arc<PlaceToTransition> {
    pub(crate) fn binding(&self) -> &str {
        &self.inner.binding
    }
}

impl Arc<TransitionToPlace> {
    pub(crate) fn evaluate(&self, token: Option<&Token>) -> Result<Token> {
        let token = self.inner.expression.evaluate(token)?;
        Ok(token)
    }

    pub(crate) fn expected_token(&self) -> Option<&str> {
        match &self.inner.expression {
            Expression::PassThrough(exp) => Some(&exp.binding_name),
            Expression::Generator(_) => None,
        }
    }
}
