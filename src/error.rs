//! Defines an error type returned as the `Err` variant from most `Results` used in this crate.

use std::fmt;

/// Catch-all error type for the crate.
#[derive(Debug)]
#[allow(missing_docs)] // documented by Display implementation
pub enum Error {
    PlaceNotFound,
    PlaceExists,
    TransitionNotFound,
    TransitionExists,
    CannotFire,
    NonDeterminism,
    Dead,
    MaxDepthReached,
    GuardCheckFailed,
    InvalidMarking,
    InvalidArcExpression,
    InvalidBinding,
    DuplicateArc,
    TokenNotFound(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::PlaceNotFound => {
                f.write_str("A place with the given name was not found in the net.")
            }
            Self::PlaceExists => f.write_str("A place with this name already exists."),
            Self::TransitionNotFound => {
                f.write_str("A transition with the given name was not found in the net.")
            }
            Self::TransitionExists => f.write_str("A transition with this name already exists."),
            Self::CannotFire => f.write_str("The transition cannot fire."),
            Self::NonDeterminism => {
                f.write_str("Non-determinism was encountered during simulation.")
            }
            Self::Dead => f.write_str("The net is dead; no transitions can fire."),
            Self::MaxDepthReached => f.write_str("The maximum state tree depth was reached."),
            Self::GuardCheckFailed => f.write_str(
                "The transition could not fire because its guard expression returned `False`.",
            ),
            Self::InvalidMarking => f.write_str("The given marking is not valid for this net."),
            Self::InvalidArcExpression => {
                f.write_str("The given expression is invalid for this arc.")
            }
            Self::InvalidBinding => f.write_str("The binding is not valid syntax."),
            Self::DuplicateArc => f.write_str("An arc between these elements already exists."),
            Self::TokenNotFound(name) => {
                write!(f, "Token with name \"{name}\" not found among input tokens",)
            }
        }
    }
}

impl std::error::Error for Error {}

/// Result type of used by most fallible functions in this crate.
pub type Result<T> = std::result::Result<T, Error>;
