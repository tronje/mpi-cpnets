//! Transition guards

use crate::token::Token;

/// A guard for a send or receive transition, e.g. MPI_Bsend, MPI_Ssend, MPI_Recv, etc.
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SendRecvGuard {
    pub src: i32,
    pub dest: i32,
    pub tag: i32,
    pub datatype: String,
}

impl SendRecvGuard {
    fn matches<S>(&self, src: i32, dest: i32, tag: i32, datatype: S) -> bool
    where
        S: AsRef<str>,
    {
        self.src == src
            && self.dest == dest
            && self.tag == tag
            && self.datatype == datatype.as_ref()
    }

    fn check_send<'a, T>(&self, tokens: T) -> bool
    where
        T: IntoIterator<Item = &'a Token>,
    {
        match tokens.into_iter().next().unwrap() {
            Token::SendMarker(sm) => self.matches(sm.src, sm.dest, sm.tag, &sm.datatype),
            _ => false,
        }
    }

    fn check_recv<'a, T>(&self, tokens: T) -> bool
    where
        T: IntoIterator<Item = &'a Token>,
    {
        let mut send_ok = false;
        let mut recv_ok = false;

        let mut count = 0;

        for token in tokens.into_iter() {
            count += 1;

            if count > 2 {
                panic!("Unexpected number of tokens!");
            }

            match token {
                Token::SendMarker(sm) => {
                    send_ok = self.matches(sm.src, sm.dest, sm.tag, &sm.datatype);
                }

                Token::RecvMarker(rm) => {
                    recv_ok = self.matches(rm.src, rm.dest, rm.tag, &rm.datatype);
                }

                _ => return false,
            }
        }

        send_ok && recv_ok
    }
}

/// A guard of a [`Transition`].
///
/// How it is evaluated depends on the type of transition guarded; that's why this is an enum.
/// Must be extended to support more MPI operations.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Guard {
    Init,
    Send(SendRecvGuard),
    Recv(SendRecvGuard),
    Allreduce,
    Finalize,
}

impl Guard {
    pub fn new_send<S>(src: i32, dest: i32, tag: i32, datatype: S) -> Self
    where
        S: Into<String>,
    {
        Self::Send(SendRecvGuard {
            src,
            dest,
            tag,
            datatype: datatype.into(),
        })
    }

    pub fn new_recv<S>(src: i32, dest: i32, tag: i32, datatype: S) -> Self
    where
        S: Into<String>,
    {
        Self::Recv(SendRecvGuard {
            src,
            dest,
            tag,
            datatype: datatype.into(),
        })
    }

    pub(crate) fn evaluate<'a, T>(&self, tokens: T) -> bool
    where
        T: IntoIterator<Item = &'a Token>,
    {
        match self {
            Self::Init => tokens.into_iter().all(Token::is_init),
            Self::Send(g) => g.check_send(tokens),
            Self::Recv(g) => g.check_recv(tokens),
            Self::Allreduce => tokens.into_iter().all(Token::is_allreduce),
            Self::Finalize => tokens.into_iter().all(Token::is_finished),
        }
    }
}
