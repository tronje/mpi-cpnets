//! Main module defining a Colored Petri Net.

use crate::arc::{ArcSpec, BindingOrExpression, PlaceToTransition, TransitionToPlace};
use crate::error::{Error, Result};
use crate::expression::Expression;
use crate::marking::Marking;
use crate::place::Place;
use crate::transition::Transition;
use rand::rngs::StdRng;
use rand::{Rng, SeedableRng};
use std::collections::HashSet;

pub(crate) mod state_iterator;

/// A Colored Petri Net (CPN).
#[derive(Debug)]
pub struct Net {
    /// The name of the net; has no functional purpose yet.
    name: String,

    /// All places in this net.
    pub(crate) places: HashSet<Place>,

    /// All transitions in this net.
    pub(crate) transitions: Vec<Transition>,

    /// PRNG used for simulations when non-determinism is encountered.
    rng: StdRng,
}

impl Net {
    /// Constructs an empty, top-level net.
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            places: HashSet::new(),
            transitions: Vec::new(),
            rng: SeedableRng::from_entropy(),
        }
    }

    /// Constructs an empty, top-level net.
    #[deprecated]
    pub fn default(name: &str) -> Self {
        Self::new(name)
    }

    /// Seed (or re-seed) the PRNG.
    ///
    /// This may be desirable to enable reproducible simulations.
    pub fn seed_rng(&mut self, seed: u64) {
        self.rng = SeedableRng::seed_from_u64(seed);
    }

    /// Get the net's name.
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Add a place with the given name to the net.
    pub fn add_place<P>(&mut self, place: P) -> Result<()>
    where
        P: Into<Place>,
    {
        let place = place.into();

        if self.places.contains(&place) {
            return Err(Error::PlaceExists);
        }

        self.places.insert(place);
        Ok(())
    }

    /// Add multiple places at once.
    pub fn add_places<P, I>(&mut self, places: I) -> Result<()>
    where
        P: Into<Place>,
        I: IntoIterator<Item = P>,
    {
        let places = places.into_iter().map(Into::into).collect::<Vec<_>>();
        for place in places.iter() {
            if self.places.contains(place) {
                return Err(Error::PlaceExists);
            }
        }

        for place in places {
            self.places.insert(place);
        }

        Ok(())
    }

    /// Get an iterator over references of the contained places.
    pub fn places(&self) -> impl Iterator<Item = &Place> {
        self.places.iter()
    }

    /// Add the given transition to the net.
    ///
    /// After it has been added, use `add_arc` to connect it to places.
    pub fn add_transition(&mut self, transition: Transition) -> Result<()> {
        if self.transitions.iter().any(|t| t.name == transition.name) {
            return Err(Error::TransitionExists);
        }

        self.transitions.push(transition);
        Ok(())
    }

    pub(crate) fn find_place(&self, name: &str) -> Option<&Place> {
        self.places.iter().find(|p| p.name() == name)
    }

    pub(crate) fn find_transition(&mut self, name: &str) -> Option<&mut Transition> {
        self.transitions.iter_mut().find(|t| t.name == name)
    }

    /// Add an arc from the place to the transition.
    pub fn add_in_arc(&mut self, from: &str, to: &str, binding: &str) -> Result<()> {
        self.find_place(from).ok_or(Error::PlaceNotFound)?;

        let transition = self.find_transition(to).ok_or(Error::TransitionNotFound)?;

        let arc = PlaceToTransition::new(from.to_owned(), binding.to_owned())?;
        transition.add_in_arc(arc)?;
        Ok(())
    }

    /// Add an arc from the transition to the place.
    pub fn add_out_arc(&mut self, from: &str, to: &str, expression: Expression) -> Result<()> {
        self.find_place(to).ok_or(Error::PlaceNotFound)?;
        let transition = self
            .find_transition(from)
            .ok_or(Error::TransitionNotFound)?;

        let arc = TransitionToPlace::new(to.to_owned(), expression);
        transition.add_out_arc(arc)?;
        Ok(())
    }

    /// Add multiple arcs at once.
    ///
    /// This function is intended to be used together with the `arcs!` macro.
    pub fn add_arcs<'a, I>(&mut self, specs: I) -> Result<()>
    where
        I: IntoIterator<Item = ArcSpec<'a>>,
    {
        for spec in specs {
            match spec.inner {
                BindingOrExpression::Binding(binding) => {
                    self.add_in_arc(spec.from, spec.to, binding)?
                }
                BindingOrExpression::Expression(exp) => {
                    self.add_out_arc(spec.from, spec.to, exp)?
                }
            }
        }

        Ok(())
    }

    /// Create an empty marking for this net.
    pub fn empty_marking(&self) -> Marking {
        Marking::empty_for(self)
    }

    /// Perform one step in a net simulation, given the marking `marking`.
    pub fn simulation_step(
        &mut self,
        marking: &mut Marking,
        allow_non_determinism: bool,
    ) -> Result<()> {
        let mut live_transitions = Vec::new();
        for t in self.transitions.iter() {
            if t.can_fire(marking)? && t.check_guard(marking)? {
                live_transitions.push(t);
            }
        }

        if live_transitions.is_empty() {
            return Err(Error::Dead);
        }

        let transition = if live_transitions.len() == 1 {
            live_transitions.pop().unwrap()
        } else if allow_non_determinism {
            let index = self.rng.gen_range(0..live_transitions.len());
            live_transitions.remove(index)
        } else {
            return Err(Error::NonDeterminism);
        };

        transition.fire(marking)
    }

    /// Repeatedly call `simulation_step` until the net is dead.
    pub fn run_simulation(
        &mut self,
        marking: &mut Marking,
        allow_non_determinism: bool,
    ) -> Result<()> {
        loop {
            match self.simulation_step(marking, allow_non_determinism) {
                Ok(marking) => marking,
                Err(e) => match e {
                    Error::Dead => break,
                    _ => return Err(e),
                },
            };
        }

        Ok(())
    }
}
