//! Functionality related to tokens.

use std::fmt;
use std::hash::Hash;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct InitMarker;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SendRecvMarker {
    pub src: i32,
    pub dest: i32,
    pub tag: i32,
    pub datatype: String,
}

impl SendRecvMarker {
    pub fn new<S>(src: i32, dest: i32, tag: i32, datatype: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            src,
            dest,
            tag,
            datatype: datatype.into(),
        }
    }

    pub fn matches<S>(&self, src: i32, dest: i32, tag: i32, datatype: S) -> bool
    where
        S: AsRef<str>,
    {
        self.src == src
            && self.dest == dest
            && self.tag == tag
            && self.datatype == datatype.as_ref()
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct AllreduceMarker;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct FinishedMarker;

/// A token in a CPN.
///
/// This is one of a number of MPI-specific marker tokens. Must be extended to simulate other MPI
/// operations.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Token {
    InitMarker,
    SendMarker(SendRecvMarker),
    RecvMarker(SendRecvMarker),
    AllreduceMarker,
    FinishedMarker,
}

impl Token {
    pub fn new_send<S>(src: i32, dest: i32, tag: i32, datatype: S) -> Self
    where
        S: Into<String>,
    {
        Self::SendMarker(SendRecvMarker {
            src,
            dest,
            tag,
            datatype: datatype.into(),
        })
    }

    pub fn new_recv<S>(src: i32, dest: i32, tag: i32, datatype: S) -> Self
    where
        S: Into<String>,
    {
        Self::RecvMarker(SendRecvMarker {
            src,
            dest,
            tag,
            datatype: datatype.into(),
        })
    }

    pub(crate) fn is_init(&self) -> bool {
        matches!(self, Self::InitMarker)
    }

    pub(crate) fn is_allreduce(&self) -> bool {
        matches!(self, Self::AllreduceMarker)
    }

    pub(crate) fn is_finished(&self) -> bool {
        matches!(self, Self::FinishedMarker)
    }
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::InitMarker => f.write_str("INIT"),
            Self::AllreduceMarker => f.write_str("ALLREDUCE"),
            Self::FinishedMarker => f.write_str("DONE"),
            Self::SendMarker(sm) => write!(
                f,
                "SEND(from={}, to={}, tag={}, type={})",
                sm.src, sm.dest, sm.tag, sm.datatype
            ),
            Self::RecvMarker(rm) => write!(
                f,
                "RECV(from={}, to={}, tag={}, type={})",
                rm.src, rm.dest, rm.tag, rm.datatype
            ),
        }
    }
}
