# MPI-specific cpnets

A Colored Petri Net implementation in Rust. Rather than the `cpnets` crate,
this does not use a modelling language at all. All tokens are a specific Rust
type, and the kind of arc expressions allowed are also limited.
